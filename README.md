# TASK MANAGER 

# DEVELOPER INFO

Name: Anastasia Kharlamova

E-MAIL: aakharlamova@tsconsulting.com

E-MAIL: xarlamovan@inbox.ru

# SOFTWARE

* JDK 11.0.9.1

* Windows OS

# HARDWARE

* RAM 16Gb

* CPU i5

* SSD 500

# BUILD PROGRAM

```
mvn clean install
```

# RUN PROGRAM

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://yadi.sk/d/V5FqXIZSB-XL0g?w=1
